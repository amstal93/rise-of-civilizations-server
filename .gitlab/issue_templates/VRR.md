## Feature description
Verification Readiness Review.

## Use cases
Check and fix if needed all documentations and code quality before release.

### Benefits
A stable release with up-to-date dependencies, and documentation.

## Requirements
- [ ] **README.md** is up-to-date
- [ ] **CHANGELOG.md** contains any functional changes and bugfixes
- [ ] **Licenses** are all verified and compatible with the software
- [ ] **Dependencies** are up-to-date
- [ ] **Code coverage** is over 80% is up-to-date
- [ ] **Nightly** build is stable

/label ~documentation ~code  ~test ~verification ~release
/assign @zsoltgyorgyszabo
