/*
 * Copyright (C) 2020 Szabo, Zsolt Gyorgy <szabo.zsolt.gyorgy@gmail.com>
 *
 * This file is part of Rise of Civilizations - Server.
 *
 * Rise of Civilizations - Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rise of Civilizations - Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rise of Civilizations - Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package us.trantor.riseofcivilizations.service;

import us.trantor.riseofcivilizations.dto.ApplicationInformation;

/**
 * Service to provide general information.
 */
public interface AboutService {

    /**
     * Provides basic information about the application.
     *
     * @return application information
     */
    ApplicationInformation getApplicationInformation();

}
